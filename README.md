Repo link:

    git clone https://github.com/nikhilkamineni/vim-config.git

Create symlinks:

    Delete original `.vimrc` config file located in home folder and then symlink the vimrc file in `~/.vim/` with the following command:
    `ln -s ~/.vim/vimrc ~/.vimrc`

To enable italics in iTerm:
    
    https://alexpearce.me/2014/05/italics-in-iterm2-vim-tmux/


Notes:

    - 'Gruvbox' colorscheme (and config info) can be found here: https://github.com/morhetz/gruvbox

